package com.example.retrofit.interfaces;

import com.example.retrofit.dtos.RequestDTO;
import com.example.retrofit.dtos.ResponseDTO;

import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.Header;
import retrofit.http.POST;


public interface SoaService {
    @POST("/api/login")
    void signin(@Header("Token") String token, @Body RequestDTO request, Callback<ResponseDTO> response);
}
