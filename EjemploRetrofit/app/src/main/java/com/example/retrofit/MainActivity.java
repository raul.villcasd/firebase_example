package com.example.retrofit;

import androidx.appcompat.app.AppCompatActivity;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.retrofit.dtos.RequestDTO;
import com.example.retrofit.dtos.ResponseDTO;
import com.example.retrofit.interfaces.SoaService;



public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private static String TAG = MainActivity.class.getName();

    private SoaService service;
    private EditText name;
    private EditText lastname;
    private EditText dni;
    private EditText email;
    private EditText password;
    private EditText commission;
    private EditText group;

    private Button button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        RestAdapter adapter = new RestAdapter.Builder()
                .setEndpoint("http://so-unlam.net.ar/api")
                .build();
        service = adapter.create(SoaService.class);

        email = findViewById(R.id.login_email);
        password = findViewById(R.id.login_password);

        button = findViewById(R.id.login_button);
        button.setOnClickListener(this);
    }


    @Override
    public void onClick(View view) {

        RequestDTO request = new RequestDTO(
                "TEST",
                email.getText().toString(),
                password.getText().toString()
        );

        service.signin("SuperToken", request, new Callback<ResponseDTO>() {
            @Override
            public void success(ResponseDTO responseDTO, Response response) {
                Log.e(TAG, responseDTO.toString());

                Intent i = new Intent(MainActivity.this, SecondActivity.class);
                i.putExtra("token", responseDTO.getToken());

                startActivity(i);
            }

            @Override
            public void failure(RetrofitError error) {
                Log.e(TAG, "Error en la conexion. ", error);
            }
        });
    }
}
